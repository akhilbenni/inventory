<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',['as'=>'login','uses'=>'LoginController@index']);
Route::get('logout',['as'=>'logout','uses'=>'LoginController@logout']);
Route::post('check-login',['as'=>'check.login','uses'=>'LoginController@checkLogin']);
Route::get('user-create',['as'=>'user.create','uses'=>'RegistrationController@index']);
Route::post('user-save',['as'=>'user.save','uses'=>'RegistrationController@userSave']);

Route::get('dashboard',['as'=>'dashboard','uses'=>'RegistrationController@dashboard']);

Route::get('products',['as'=>'products','uses'=>'ProductController@index']);
Route::post('product-save',['as'=>'product.save','uses'=>'ProductController@productsSave']);

Route::get('assign-product',['as'=>'assign.product','uses'=>'ProductAssignController@index']);
Route::post('assign-product-save',['as'=>'assign.product.save','uses'=>'ProductAssignController@assignProductSave']);
Route::group(['as'=>'shops.'],function(){
    Route::get('my-products',['as'=>'my.products','uses'=>'ProductController@myProducts']);
    
});
Route::group(['as'=>'user.'],function(){
    Route::get('purchase-products',['as'=>'purchase.products','uses'=>'ProductAssignController@purchaseProducts']);
    Route::post('get-shop-products',['as'=>'shops.get.products','uses'=>'ProductAssignController@getShopProducts']);
    Route::post('purchase-product-save',['as'=>'purchase.product.save','uses'=>'ProductAssignController@purchaseShopProductSave']);
});

