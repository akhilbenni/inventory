$(document).ready(function () {
    $(".sidebar-dropdown > a").click(function () {
        $(".sidebar-submenu").slideUp(200), $(this).parent().hasClass("active") ? ($(".sidebar-dropdown").removeClass("active"), $(this).parent().removeClass("active")) : ($(".sidebar-dropdown").removeClass("active"), $(this).next(".sidebar-submenu").slideDown(200), $(this).parent().addClass("active"))
    }), $("#close-sidebar").click(function () {
        $(".page-wrapper").removeClass("toggled")
    }), $("#show-sidebar").click(function () {
        $(".page-wrapper").addClass("toggled")
    })
});
var tabLinks = document.querySelectorAll(".tablinks"), tabContent = document.querySelectorAll(".tabcontent");

function openTabs(e) {
    var t = e.currentTarget, i = t.dataset.country;
    tabContent.forEach(function (e) {
        e.classList.remove("active")
    }), tabLinks.forEach(function (e) {
        e.classList.remove("active")
    }), document.querySelector("#" + i).classList.add("active"), t.classList.add("active")
}

tabLinks.forEach(function (e) {
    e.addEventListener("click", openTabs)
});
var SITE = SITE || {};

function readURL(e) {
    if (e.files && e.files[0]) {
        var t = new FileReader, i = URL.createObjectURL(event.target.files[0]);
        t.onload = function (e) {
            $("#img-uploaded").attr("src", e.target.result), $("input.img-path").val(i)
        }, t.readAsDataURL(e.files[0])
    }
}

SITE.fileInputs = function () {
    var e = $(this), t = e.val().split("\\"), i = t[t.length - 1], a = e.siblings(".btn"),
        n = e.siblings(".file-holder");
    "" !== i && (a.text("Photo Chosen"), 0 === n.length ? a.after('<span class="file-holder">' + i + "</span>") : n.text(i))
}, $(".file-wrapper input[type=file]").bind("change focus click", SITE.fileInputs), $(".uploader").change(function () {
    readURL(this)
}), $(function () {
    $('[data-toggle="tooltip"]').tooltip()
}), $(document).ready(function () {
    $(function () {
        $(".sidbar_li").each(function () {
            window.location.href, $(this).children().prop("href") == window.location.href && $(this).children().addClass("activ")
        }), $(".sidebar-dropdown .sidebar-submenu ul li").each(function () {
            window.location.href, $(this).children().prop("href") == window.location.href && $(this).children().addClass("subactiv").closest(".sidebar-submenu").addClass("blockmenu").closest(".sidebar-dropdown").addClass("activ2")
        })
    })
})/*, tinymce.init({
    forced_root_block: "",
    selector: "textarea.tiny",
    height: 100,
    branding: !1,
    theme: "modern",
    plugins: ["advlist autolink lists link image charmap print preview hr anchor pagebreak", "searchreplace wordcount visualblocks visualchars code fullscreen", "insertdatetime media nonbreaking save table contextmenu directionality", "emoticons template paste textcolor colorpicker textpattern imagetools codesample toc fullpage"],
    toolbar1: "undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code",
    image_advtab: !0,
    fullpage_hide_in_source_view: !0,
    browser_spellcheck: !0,
    templates: [{title: "Description", content: "Description"}]
});*/