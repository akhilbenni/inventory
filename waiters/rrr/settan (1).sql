-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2021 at 11:57 PM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `settan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `hotel_name` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` int(11) NOT NULL,
  `contact_number` int(11) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `password` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `admin_id`, `hotel_name`, `name`, `email`, `contact_number`, `phone_number`, `role`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 0, 'sarovaram', 'aaaa', 0, 0, 0, 0, 0, 0, 0, 0, 0),
(2, 0, 'Abhilash', 'sss', 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` bigint(20) UNSIGNED NOT NULL,
  `hotel_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_number` bigint(20) DEFAULT NULL,
  `phone_number` bigint(20) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT 2,
  `remember_token` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `hotel_name`, `name`, `email`, `contact_number`, `phone_number`, `role`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Sagar', 'Sagar', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL),
(2, 'Thaj', 'Thaj', NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`id`, `name`) VALUES
(1, 'Biriyani'),
(2, 'Meals\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `user_type` enum('USER','VENDOR','ADMIN','SUBADMIN') DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `restuarant` varchar(200) NOT NULL DEFAULT current_timestamp(),
  `otp` int(11) NOT NULL,
  `otp_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `user_type`, `name`, `mobile`, `email`, `password`, `status`, `restuarant`, `otp`, `otp_count`) VALUES
(1, 'ADMIN', '1', '1234567890', 'admin@gmail.com', 'efpb4m3SSISt6', 1, '2020-02-21 12:01:31', 0, 0),
(2, 'ADMIN', 'Abindev', '444444444', 'abin@gmail.com', '$2y$10$jDd2IwaJmPAfO/vJeP5CmehFqxvRsu8oqqtuiMqFE6cjxc8gSzUey', 1, '1', 0, 0),
(3, 'ADMIN', 'damu', '23222', 'damu@gmail.com', 'efpp2bUsfJUAU', 1, '2', 0, 0),
(4, 'ADMIN', 'benny', '999999999', 'benny@gmail.com', 'efIx/Htg4lvDg', 1, '2', 0, 0),
(5, 'ADMIN', 'sakkariya', '333333333333', 'sakkariya@gmail.com', 'efpp2bUsfJUAU', 1, '2', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `orderdetails`
--

CREATE TABLE `orderdetails` (
  `id` int(11) NOT NULL,
  `tablename` varchar(100) NOT NULL,
  `staffid` int(11) NOT NULL,
  `restuarant` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `tstamp` timestamp NOT NULL DEFAULT current_timestamp(),
  `food` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderdetails`
--

INSERT INTO `orderdetails` (`id`, `tablename`, `staffid`, `restuarant`, `status`, `tstamp`, `food`) VALUES
(1, 'Table-1', 4, 1, 0, '2021-06-09 18:55:40', 0),
(2, 'Table-2', 4, 1, 1, '2021-06-09 18:55:40', 0),
(3, '1', 4, 0, 0, '2021-06-09 21:31:59', 1),
(4, '2', 4, 0, 1, '2021-06-09 21:33:27', 2),
(5, '1', 4, 0, 1, '2021-06-09 21:33:40', 2),
(6, '2', 4, 0, 1, '2021-06-09 21:36:56', 2),
(7, '1', 4, 0, 1, '2021-06-09 21:37:04', 2),
(8, '1', 5, 0, 1, '2021-06-09 21:48:25', 2),
(9, '2', 5, 2, 0, '2021-06-09 21:51:02', 2),
(10, '1', 5, 2, 0, '2021-06-09 21:52:52', 2),
(11, '2', 5, 2, 0, '2021-06-09 21:54:52', 2),
(12, '2', 5, 2, 0, '2021-06-09 21:55:30', 2);

-- --------------------------------------------------------

--
-- Table structure for table `originalorder`
--

CREATE TABLE `originalorder` (
  `id` int(11) NOT NULL,
  `staff` int(11) NOT NULL,
  `date` date NOT NULL,
  `tableid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `originalorder`
--

INSERT INTO `originalorder` (`id`, `staff`, `date`, `tableid`) VALUES
(1, 3, '0000-00-00', 1),
(2, 3, '0000-00-00', 2),
(3, 4, '0000-00-00', 2),
(4, 4, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tables`
--

CREATE TABLE `tables` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `rest` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tables`
--

INSERT INTO `tables` (`id`, `name`, `rest`, `status`) VALUES
(1, 'Table-1', 2, 0),
(2, 'Table-2', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderdetails`
--
ALTER TABLE `orderdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `originalorder`
--
ALTER TABLE `originalorder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tables`
--
ALTER TABLE `tables`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `orderdetails`
--
ALTER TABLE `orderdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `originalorder`
--
ALTER TABLE `originalorder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tables`
--
ALTER TABLE `tables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
