<?php

Class Login_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function login($email,$password)
    {
    	$qry="SELECT * FROM login where email=? and password=? and status=1";
        $qry = $this->db->query($qry,array($email,$password));
        
        
        if($qry->num_rows()>0){
            
            return $res=$qry->row_array();        

        }else{

            return array();
        }
    }

function getdata($table,$order,$where)
    {
        if (empty($where)) {
        
            $qry="SELECT * FROM $table  $order";

        }else{

            $qry="SELECT * FROM $table $where $order";

        }

        
        $qry = $this->db->query($qry);
        
        
        if($qry->num_rows()>0){
            
            return $qry->result_array();        

        }else{

            return array();
        }
    }

     function loginseller($email,$password)
    {
        $qry="SELECT * FROM seller where email=? and password=? and status=0";
        $qry = $this->db->query($qry,array($email,$password));
        
        
        if($qry->num_rows()>0){
            
            return $res=$qry->row_array();        

        }else{

            return array();
        }
    }

      
    }