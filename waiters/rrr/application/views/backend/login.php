<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta charset="UTF-8">
    <title>Admin Panel</title>
    <link rel="shortcut icon" href="<?php echo base_url();?>images/fav.png">
    <link href="<?php echo base_url('assets/');?>css/login.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
</head>
<body>
<div class="Loginwrapper">
    <div class="container">
        <!-- <img src="<?php echo base_url();?>images/logo.png"> -->
        <h1>Admin Panel Login</h1>
        
        <form class="form" method="POST" id="login_form">
            <input type="text" placeholder="Username" name="email">
            <input type="password" placeholder="Password" name="password">
            <p class="Wrongalert hide"> Wrong Username or Password</p>

            <button type="submit" id="login_button">Login</button>
        </form>
    </div>
    <ul class="bg-bubbles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
</div>
<div class="clearfix"></div>
<script src="<?php echo base_url('assets/') ?>js/jquery-1.12.4.min.js"></script>


<script type="text/javascript">

   $("#login_button").on('click',function(e)
   {

        e.preventDefault();
        
        var data= $("#login_form").serializeArray();
        
        $.post('<?php echo base_url()?>login',data,function(data)
        {
          
            if(data.status)
            {
             window.location = '<?php echo base_url() ?>backend';               
            }
            else
            {
                   alert(data.reason);
            }


        },
        'json');
    });

</script>
</body>
</html>