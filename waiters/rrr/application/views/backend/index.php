<?php echo $header; ?>
</head>

<body>
<?php echo $nav; ?>
<!-- nav.php ends  -->

<div class="cardpad" style="height: auto">
    <div class="p_t5">
        <div class="col-md-3 pad5">
            <div class="card mini-stat lightblue text-white">
                
            </div>
        </div> 
         
 
    </div>
    <div class="clearfix"></div>

</div>

<!--       ************************************************************-->
<div class="clearfix"></div>


<div class="cardpad" style="height: auto">

    <!--    ===========================-->
    <!-- <div class="p_t5">
        <div class="col-md-6 pad5">
            <div class="card mini-stat text-white">
                <div class="box-header bgsecndary with-border p-l-15 p-r-15 text-center">
                    <h4 class="box-title font-500"> Out of Stock Items </h4>
                </div>
                <div class="card-body">
                    <table id="example" class="table table-striped" style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Fish Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>System Architect</td>
                        </tr>

                        <tr>
                            <td>2</td>
                            <td>System Architect</td>
                        </tr>

                        <tr>
                            <td>3</td>
                            <td>System Architect</td>
                        </tr>

                        <tr>
                            <td>4</td>
                            <td>System Architect</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-index pull-right">
                        View All
                    </button>
                </div>
            </div>
        </div>
        

        <div class="col-md-6 pad5">
            <div class="card mini-stat text-white">
                <div class="box-header bgsecndary with-border p-l-15 p-r-15 text-center">
                    <h4 class="box-title font-500"> Limited Stock Items </h4>
                </div>
                <div class="card-body">
                    <table id="example" class="table table-striped" style="width:100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Fish Name</th>
                            <th>Available Quantity</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>System Architect</td>
                            <td>
                                500kg
                            </td>
                        </tr>

                        <tr>
                            <td>2</td>
                            <td>System Architect</td>
                            <td>
                                20kg
                            </td>
                        </tr>

                        <tr>
                            <td>3</td>
                            <td>System Architect</td>
                            <td>
                                200kg
                            </td>
                        </tr>

                        <tr>
                            <td>4</td>
                            <td>System Architect</td>
                            <td>
                                100kg
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button class="btn btn-index pull-right">
                        View All
                    </button>
                </div>
            </div>
        </div>
        
    </div> -->
</div>

<!--       ************************************************************-->
<div class="clearfix"></div>

<?php echo $footer; ?>

</body>

</html>