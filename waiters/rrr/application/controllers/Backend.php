<?php 

class Backend extends CI_Controller
{
	  public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session','form_validation'));
        $this->load->helper(array('url','form','string'));    
        $this->load->model('Backend_model');

        $session = $this->session->userdata('login_cred');

         if(!$session)
         {
            redirect('/','refresh');         

         }  

    }

        public function menu(){
         
        $data['header'] = $this->load->view("backend/header", "", true);              
        $data['nav']    = $this->load->view("backend/nav", "", true);
        $data['footer'] = $this->load->view("backend/footer","",true);    

        return $data;
        }


      function index()
      {
         
         $data =$this->menu();
         $this->load->view('backend/index',$data);
      }

     

       function order()
      {
        

        $data =$this->menu();

        $data['users']=$this->Backend_model->gettables();

        //echo "<pre>"; print_r( $this->session->userdata('login_cred')); exit;
        $data['food']=$this->Backend_model->getfood();
        $data['table']=$this->Backend_model->getdata('tables','order by id desc' ,'WHERE status=0');

        $this->load->view('backend/users',$data);
      }


         function takeorder()
       {
        if($this->input->is_ajax_request()){
            
                $session = $this->session->userdata('login_cred');

                $id = $session['id'];
                $rest = $session['rest'];

                //print_r($_POST); exit;

                
                $time= time();
              //  $array=array('restuarant' => $rest,'tableid' => $this->input->post('id'),'staff' => $id,'date' => $time);

                

                
                //$result = $this->db->insert('originalorder',$array);

                $tblid = $this->input->post('tblid');
                //print_r($tblid); exit;
                $this->db->where('id',$tblid);
                $result=  $this->db->update('tables', array('status' => 0));                

                $this->db->where('id',$this->input->post('id'));
                $this->db->update('orderdetails', array('status' => $this->input->post('value')));
                


                if($result){
                    
                        exit(json_encode(array('status'=>true)));
                    
                }else{
                    exit(json_encode(array('status'=>false,'reason'=>'Please try with a valid login')));
                }
           
        }else{

            exit(json_encode(array('status'=>false,'reason'=>"This request not allowed")));
        }
    }   




    function addorder()
       {
        if($this->input->is_ajax_request()){
            
                $session = $this->session->userdata('login_cred');

                $id = $session['id'];

                //print_r($_POST); exit;
                 $rest = $session['rest'];
                
                $time= time();
                $array=array('tablename' => $this->input->post('tables'),'staffid' => $id,'food' => $this->input->post('food'),'status' =>1,'restuarant' =>$rest);

                
                $result = $this->db->insert('orderdetails',$array);

                 $this->db->where('id',$this->input->post('tables'));
                $this->db->update('tables', array('status' => 1));


                if($result){
                    
                        exit(json_encode(array('status'=>true)));
                    
                }else{
                    exit(json_encode(array('status'=>false,'reason'=>'Please try with a valid login')));
                }
           
        }else{

            exit(json_encode(array('status'=>false,'reason'=>"This request not allowed")));
        }
    }   
     
     
     
    	function logout()
    	{
    		$this->session->unset_userdata('logged_in');
    		
            redirect('/Login');
    	}


}
?>