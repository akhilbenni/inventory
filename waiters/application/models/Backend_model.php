<?php

Class Backend_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
    function login($email,$password)
    {
    	$qry="SELECT * FROM login where email=? and password=? and status=1";
        $qry = $this->db->query($qry,array($email,$password));
        
        
        if($qry->num_rows()>0){
            
            return $res=$qry->row_array();        

        }else{

            return array();
        }
    }

    function getdata($table,$order,$where)
    {
        if (empty($where)) {
        
            $qry="SELECT * FROM $table  $order";

        }else{

            $qry="SELECT * FROM $table $where $order";

        }

        
        $qry = $this->db->query($qry);
        
        
        if($qry->num_rows()>0){
            
            return $qry->result_array();        

        }else{

            return array();
        }
    }

     
    function gettablesList()
    {
         $session = $this->session->userdata('login_cred');

         $id = $session['id'];
         $rest = $session['rest'];

        
        $qry="SELECT `hotel_table_id`, `table_name`, `chair_count`, `status`, `hotel_id`, `created_at`, `updated_at` 
        FROM `hotel_tables` WHERE  hotel_id='$rest' AND `status`=1 ";
       
        $qry = $this->db->query($qry);        
        
        if($qry->num_rows()>0){
            
            return $qry->result_array();        

        }else{

            return array();
        }
    }

    function gettables()
    {
         $session = $this->session->userdata('login_cred');

         $id = $session['id'];


        
        $qry="SELECT o.*,t.table_name as tablename, t.hotel_table_id as tid,l.name,a.hotel_name FROM orderdetails o 
        JOIN login l ON o.staffid=l.id 
        JOIN admins a ON a.admin_id=o.restuarant 
        JOIN hotel_tables t ON t.hotel_table_id = o.tablename 
        WHERE o.staffid='$id' AND o.status=1";
       
        $qry = $this->db->query($qry);        
        
        if($qry->num_rows()>0){
            
            return $qry->result_array();        

        }else{

            return array();
        }
    }

    function getOrderDetails()
    {
         $session = $this->session->userdata('login_cred');

         $id = $session['id'];


        
        $qry="SELECT o.*,t.table_name as tablename, t.hotel_table_id as tid,l.name,a.hotel_name,f.name as foodName,f.price as foodPrice FROM orderdetails o 
        JOIN login l ON o.staffid=l.id 
        JOIN admins a ON a.admin_id=o.restuarant 
        JOIN hotel_tables t ON t.hotel_table_id = o.tablename 
        JOIN hotel_foods f ON f.hotel_food_id = o.food 
        WHERE o.staffid='$id' AND o.status=1";
       
        $qry = $this->db->query($qry);        
        
        if($qry->num_rows()>0){
            
            return $qry->result_array();        

        }else{

            return array();
        }
    }

    function getfood()
    {
         $session = $this->session->userdata('login_cred');

         $id = $session['rest'];


        
        $qry="SELECT * FROM hotel_foods WHERE hotel_id='$id'";
       
        $qry = $this->db->query($qry);        
       // print_r($this->db->last_query()); exit;
        if($qry->num_rows()>0){
            
            return $qry->result_array();        

        }else{

            return array();
        }
    }


}