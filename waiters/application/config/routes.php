<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//echo CI_VERSION; exit();
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']   = 'Login';

$route['404_override']         = '';
$route['translate_uri_dashes'] =  FALSE;

//$route['(:any)'] = 'Frontend/$1';
$route['login']                = 'Login/validate_here';
$route['backend']              = 'Backend/index';
$route['product']              = 'Backend/product';
$route['vendor']               = 'Backend/vendor';
$route['unit']                 = 'Backend/unit';
$route['stock']           = 'Backend/stock';
$route['purchase']        = 'Backend/purchase';
$route['add-purchase']    = 'Backend/add_purchase';
$route['add-sale']    = 'Backend/add_sale';
$route['sale']    = 'Backend/sale';
$route['return-sale']    ='Backend/return_sale';
$route['add-return']    ='Backend/add_return';
$route['sale-report']    ='Backend/sale_report';
$route['customer-report']    ='Backend/customer_report';
$route['stock-report']    ='Backend/stock_report';
$route['logout']             ='Login/logout';
$route['ins_product']           = 'Backend/ins_product';
$route['delete_product/(:any)'] = 'Backend/delete_product/$id';
$route['ins_purchase']           = 'Backend/ins_purchase';
$route['delete_purchase/(:any)'] = 'Backend/delete_purchase/$id';
$route['update_product']           = 'Backend/update_product';
$route['update_purchase']           = 'Backend/update_purchase';
$route['updatestock']           = 'Backend/updatestock';


//FRONTEND

$route['userregistration']                = 'Frontend/registeruser';
$route['signinuser']                      = 'Frontend/loginuser';
$route['sellerregistration']              = 'Frontend/addseller';
$route['sellerlogin']                     = 'Frontend/loginseller';
$route['seller-signup']                   = 'Frontend/sell_signup';
$route['user-signin']                     = 'Frontend/userlogin';
$route['shop/(:any)']                     = 'Frontend/shop/$id';
$route['products/(:any)']                 = 'Frontend/shoptwo/$id';