<?php echo $header; ?>
</head>

<body>
<?php echo $nav; ?>
<!-- nav.php ends  -->
<div class="cardpad">
    <div class="box-header with-border p-l-15 p-r-15">
        <h4 class="box-title"><i class="mdi mdi-store"></i> Seller </h4>

        <button class="btn  pull-right modalCommonBtn" data-toggle="modal" data-target="#addstock">
            <i class="mdi mdi-plus"></i> Add
        </button>
    </div>

    <div class="boxShadaw">
        <table id="example" class="table table-striped" style="width:100%">
            <thead>
            <tr>
                <th class="TdNumber">#</th>
                <th>Table Number</th>
                <th>Food</th>
                <th>Status</th>
                <th>Action</th>
                
                
            </tr>
            </thead>
            <tbody>
                
            <?php if (!empty($users)) {
                // code...

                //echo "<pre>"; print_r($users); exit;
             foreach ($users as $key => $value) { ?>

            <tr id="row<?php echo $value['id'];?>">
                 <td><?php echo $key+1;?></td>
                 <td class="name"><?php echo $value['tablename'];?></td>
                 
<td><?php echo $value['foodName'];?> </td>
<td><?php echo $value['foodPrice'];?> </td>
                 <td class="mobile"><?php if($value['status']==0){ echo "Free";}else{  echo "Engaged";} ?></td>
                 <td class=""><?php if($value['status']==0){ ?> <button data-id="<?php echo $value['id'];?>" class="btn btn-success abcd" data-tabl="<?php echo $value['tid'];?>" data-value="1">Order Now</button><?php }else{  ?> <button data-id="<?php echo $value['id'];?>" data-tabl="<?php echo $value['tid'];?>"  class="btn btn-success abcd" data-value="0">Print Bill</button><?php } ?></td>
            </tr>

            <?php }  } ?>

            </tbody>
        </table>
    </div>
</div>
<!--**********   Footer ends ****************-->

<div class="clearfix"></div>
<!-- container-fluid  end -->

<div class="clearfix"></div>
<!-- container-fluid  end -->
<div id="addstock" class="modal fade" role="dialog" data-backdrop="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add</h4>
            </div>
            <form action="<?php echo base_url('Backend/addorder');?>" method="post" enctype="multipart/form-data" id="formid">
                <div class="modal-body" >
                  
 
                    <div class="col-md-6 pad5">
                        <div class='form-group RequirdField'>
                            <label class="">Food</label>
                           <select id="food" name="food" class="form-control" >

                    <?php foreach ($food as $key => $value) {
                        
                    ?>

                    <option value="<?php  echo $value['hotel_food_id']; ?>"><?php  echo $value['name']; ?></option>
                <?php  } ?>

                </select> 
                            <p class="error_message hide">Please fill this field</p>
                        </div>
                    </div>
                    <div class="col-md-6 pad5">
                        <div class='form-group RequirdField'>
                            <label class="">Table</label>
                            <select id="food" name="tables" class="form-control" >
                            <option value="">Select Table</option>
                                <?php foreach ($table as $key => $value) { ?>

                                    <option value="<?php  echo $value['hotel_table_id']; ?>"><?php  echo $value['table_name']; ?></option>

                                <?php  } ?>

                        </select> 
                            <p class="error_message hide">Please fill this field</p>
                        </div>
                    </div>
                     

                    <input type="hidden" name="id" value="0" id="id" >
                </div>
                <div class="modal-footer">
                    <button class="btn btn-info pull-right" type="submit">Save</button>
                    <button class="btn btn-default pull-right m-r-3" data-dismiss="modal" type="submit">Cancel
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php echo $footer; ?>


<script type="text/javascript">
    
    $(document).on('click', '.abcd', function(){
        
        if (confirm("Are you sure?")) {

        var cur = $(this);

        var id = $(this).data('id');
        var value = $(this).data('value');
        var tabl = $(this).data('tabl');


         $.post("<?php echo base_url();?>Backend/takeorder", { ajax: true,id:id,value:value, tblid:tabl}, function(data) {

         
 
          
          if(data.status==true){


            
            
            alert('Action taken successfully');
            location.reload();


       }else{

         alert('Please try again');
         return;
       }
     
        
      

      }, "json");


 }else{


  return false;
 }

    });


  </script>



<script type="text/javascript">
   
            var teacher_form = jQuery("#formid").validate({

            ignore: [],
            errorElement: "span",
            errorClass: "help-inline-error",
            errorPlacement: function(error, element) 
            {
                
            },
            submitHandler: function(datas) {
                
                
                    $('.body_blur').show();
                    jQuery(datas).ajaxSubmit({
                        dataType: "json",
                        success: function(data) {
                            $('.body_blur').hide();
                            if (data.status) {
                                
                                
                                alert(data.reason);
                                setTimeout(function() {
                                location.reload();
                                    }, 1500);
                                return false;

                            } else {
                               
                                alert(data.reason);
                                setTimeout(function() {
                               
                                }, 2000);
                            }
                        }
                    });
                
            }
        });

</script>


<script>
    $(document).ready(function () {
        $('#example').DataTable();
    });
</script>
</body>

</html>