<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <meta charset="UTF-8">
    <title>Admin Panel</title>
    <link rel="shortcut icon" href="<?php echo base_url();?>images/favicon.png">
    <link href="<?php echo base_url('assets/');?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/');?>fonts/material-design-iconic-font/css/materialdesignicons.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>css/bootstrap-datetimepicker.min.css">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link href="<?php echo base_url('assets/');?>css/adminmaster.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url('assets/');?>css/dataTables.bootstrap.min.css">
     <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap" rel="stylesheet">
     <style>
    
    .success_msg{
        color: #fff;
    background-color: #398439;
    border-color: #255625;
    padding: 10px;
    text-align: center;}

        .danger_msg{
        color: #fff;
    background-color: ##d43f3a;
    border-color: #255625;
    padding: 10px;
    text-align: center;}
</style>