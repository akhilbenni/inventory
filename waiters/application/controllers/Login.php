<?php 

class Login extends CI_Controller
{
		public function __construct()
    {
        parent::__construct();
        $this->load->library(array('session','form_validation'));
        $this->load->helper(array('url','form','string'));    
        $this->load->model('Login_model');

    }

        public function menu(){
         
        $data['header'] = $this->load->view("backend/header", "", true);              
        $data['nav'] = $this->load->view("backend/nav", "", true);
        $data['footer'] = $this->load->view("backend/footer","",true);    

        return $data;
    }


      function index()
      {
         
         $data =$this->menu();
         $this->load->view('backend/login',$data);
      }

       function register()
      {
         
         $data =$this->menu();
         $data['rest']=$this->Login_model->getdata('admins','order by admin_id desc' ,'');
         $this->load->view('backend/register',$data);
      }
		

		function staff()
	   {
		if($this->input->is_ajax_request()){

      		$this->form_validation->set_rules("email",'Email','trim|required|valid_email');
      		$this->form_validation->set_rules("password",'Password','trim|required');
      		$this->form_validation->set_rules("mobile",'mobile','trim|required');
      		$this->form_validation->set_rules("restuarant",'restuarant','trim|required');
      		$this->form_validation->set_rules("name",'name','trim|required');

      	
      		if($this->form_validation->run() === TRUE){

	          	$email=$this->input->post('email');
	          
 	            //$password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
 	            $password = crypt($this->input->post('password'),'efish@#$123');


 					

                 
             	$array= array('name' => $this->input->post('name'),'email' => $this->input->post('email'),'password' => $password,'mobile' => $this->input->post('mobile'),'restuarant' => $this->input->post('restuarant'),'user_type'=>'ADMIN');

	          	$result = $this->db->insert('login',$array);

	          	if($result){
	          		
		              	exit(json_encode(array('status'=>true,'reason'=>'Registered successfully')));
		            
	          	}else{
	            	exit(json_encode(array('status'=>false,'reason'=>'Please try with a valid login')));
	          	}
	      	}else{
	        	exit(json_encode(array('status'=>false,'reason'=>strip_tags(validation_errors()))));
	      	}
		}else{

		    exit(json_encode(array('status'=>false,'reason'=>"This request not allowed")));
		}
	}
    


	   function validate_here()
	   {
		if($this->input->is_ajax_request()){
      		$this->form_validation->set_rules("email",'Email','trim|required|valid_email');
      		$this->form_validation->set_rules("password",'Password','trim|required');

      	
      		if($this->form_validation->run() === TRUE){

	          	$email=$this->input->post('email');
	          
 	            $password = crypt($this->input->post('password'),'efish@#$123');


 					//echo $password;exit('d');
				 //print_r(crypt($this->input->post('password'),'efish@#$123')); exit();

                 
             	
	          	$result = $this->Login_model->login($email,$password);
				// print_r($result);
				// exit();
	          	if($result){
	          		$id = $result['id'];
	          		
	          		  	$session_data=array();
		              	
		              	$session_data=array(
			                  'id'=>$result['id'],
			                  'email'=>$result['email']	,
			                  'type'=>'ADMIN','rest'=>$result['restuarant']	);
			            
			            $result=$this->session->set_userdata('login_cred',$session_data);// $this->session->
		              	exit(json_encode(array('status'=>true)));
		            
	          	}else{
	            	exit(json_encode(array('status'=>false,'reason'=>'Please try with a valid login')));
	          	}
	      	}else{
	        	exit(json_encode(array('status'=>false,'reason'=>strip_tags(validation_errors()))));
	      	}
		}else{

		    exit(json_encode(array('status'=>false,'reason'=>"This request not allowed")));
		}
	}




	   function checksellerlogin()
	   {
		if($this->input->is_ajax_request()){
      		$this->form_validation->set_rules("email",'Email','trim|required|valid_email');
      		$this->form_validation->set_rules("password",'Password','trim|required');

      	
      		if($this->form_validation->run() === TRUE){

	          	$email=$this->input->post('email');
	          
 	           $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);


 					//echo $password;exit('d');
				 //print_r(crypt($this->input->post('password'),'efish@#$123')); exit();

                 
             	
	          	$result = $this->Login_model->loginseller($email,$password);

	          	if($result){
	          		$id = $result['id'];
	          		
	          		  	$session_data = array();
		              	
		              	$session_data = array('id'=>$result['id'],'email'=>$result['email'],'type'=>'SELLER');
			            
			            $result=$this->session->set_userdata('login_cred',$session_data);// $this->session->
		              	exit(json_encode(array('status'=>true)));
		            
	          	}else{
	            	exit(json_encode(array('status'=>false,'reason'=>'Please try with a valid login')));
	          	}
	      	}else{
	        	exit(json_encode(array('status'=>false,'reason'=>strip_tags(validation_errors()))));
	      	}
		}else{

		    exit(json_encode(array('status'=>false,'reason'=>"This request not allowed")));
		}
	}

      
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		
        redirect('/Login');
	}


	


	
}
?>