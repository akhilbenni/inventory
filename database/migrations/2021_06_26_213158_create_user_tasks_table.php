<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_tasks', function (Blueprint $table) {
            $table->bigIncrements('user_task_id');
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->unsignedBigInteger('task_id')->nullable();
            $table->timestamps();
            $table->foreign('admin_id')->references('admin_id')->on('admins');
            $table->foreign('task_id')->references('task_id')->on('tasks');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_tasks');
    }
}
