<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTaskTimingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_task_timings', function (Blueprint $table) {
            $table->bigIncrements('user_task_timing_id');
            $table->unsignedBigInteger('user_task_id')->nullable();
            $table->foreign('user_task_id')->references('user_task_id')->on('user_tasks');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_task_timings');
    }
}
