<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('password'),
            'role' => 0,
            'status' => 1
        ]);
    }
}
