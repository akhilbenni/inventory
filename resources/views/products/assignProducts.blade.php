@extends('layout.inner_layout')
@section('title','Assign')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">General Form</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add table Details</h3>
                        </div>
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @if(session()->has('error'))
                            <div class="alert alert-danger">
                                <ul>{{session()->get('error')}}</ul>
                            </div>
                        @endif
                         @if(session()->has('message'))
                            <div class="alert alert-success">
                                <ul>{{session()->get('message')}}</ul>
                            </div>
                        @endif
                        
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{route('assign.product.save')}}" id="staff_form" class="staff_form" method="post">
                            {!! csrf_field() !!}
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="col-md-4">
                                    <label for="exampleInputEmail1">Product </label>
                                    <select class="form-control" id="product_id" name="product_id">
                                    <option value=""></option>
                                    @foreach ($products as $product)
                                        <option value="{{ $product->product_id}}">{{ $product->product_name}}</option>
                                    @endforeach
                                    </select>
                                    </div>
                                    <div class="col-md-4">
                                    <label for="exampleInputEmail1">Shops</label>
                                    <select class="form-control" id="inventory_id" name="inventory_id">
                                    <option value=""></option>
                                    @foreach ($admins as $admin)
                                        <option value="{{ $admin->admin_id}}">{{ $admin->name}}</option>
                                    @endforeach
                                    </select>
                                    </div>
                                    <div class="col-md-4">
                                    <label for="exampleInputEmail1">Quantity</label>
                                    <input type="text" class="form-control" name="qty" id="qty">
                                    </div>
                                </div>
                            </div>
                            
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
                    <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>S/No</th>
                            <th>Shope</th>
                            <th>Product</th>
                            <th>Quantity</th>
                          </tr>
                          </thead>
                         <tbody>
                             @foreach($assignedProducts as $assignedProduct)
                             <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $assignedProduct->admin->name }}</td>
                                <td>{{ $assignedProduct->product->product_name }}</td>
                                <td>{{ $assignedProduct->quantity }}</td>
                              </tr>
                             @endforeach
                         </tbody>
                        </table>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('public/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('public/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@push('js')
<script src="{{ asset('public/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('public/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('public/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('public/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
@endpush
