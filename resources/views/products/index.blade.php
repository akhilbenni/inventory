@extends('layout.inner_layout')
@section('title','Task')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">General Form</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add table Details</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{route('product.save')}}" id="staff_form" class="staff_form" method="post">
                            {!! csrf_field() !!}
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="col-md-4">
                                    <label for="exampleInputEmail1">Product Name</label>
                                    <input type="text" class="form-control" id="product_name" name="product_name"
                                        placeholder="Product Name">
                                    </div>
                                    <div class="col-md-4">
                                    <label for="exampleInputEmail1">Product ID</label>
                                    <input type="text" class="form-control" id="product_code" name="product_code"
                                        placeholder="Product ID">
                                    </div>
                                    <div class="col-md-4">
                                    <label for="exampleInputEmail1">Capacity of product</label>
                                    <input type="text" class="form-control" id="product_capacity" name="product_capacity"
                                        placeholder="Capacity of product">
                                    </div>
                                    
                                    
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>

                    </div>
                </div>
                    <div class="col-md-12">
                    <div class="card">
                    <div class="card-body">
                        <table id="example1" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>S/No</th>
                            <th>Inventory id</td>
                                <th>Product Name</td>
                                <th>Product Code</th>
                                <th>Product Capacity</th>
                          </tr>
                          </thead>
                         <tbody>
                             @foreach($products as $product)
                             <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $product->inventory_id }}</td>
                                <td>{{ $product->product_name }}</td>
                                <td>{{ $product->product_code }}</td>
                                <td>{{ $product->product_capacity }}</td>
                                
                              </tr>
                             @endforeach
                         </tbody>
                        </table>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
@push('css')
<link rel="stylesheet" href="{{ asset('public/admin/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{ asset('public/admin/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
@push('js')
<script src="{{ asset('public/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ asset('public/admin/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{ asset('public/admin/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{ asset('public/admin/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
@endpush
