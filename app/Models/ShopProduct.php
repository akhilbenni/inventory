<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopProduct extends Model
{
    protected $table = 'shops_products';
    protected $primaryKey = 'shop_product_id';
    protected $fillable = ['inventory_id','product_id','quantity','status','created_by'];

    public function product(){
        return $this->hasOne(Product::class,'product_id','product_id');
    }
    public function admin(){
        return $this->hasOne(Admin::class,'admin_id','inventory_id');
    }
}
