<?php

namespace App\Http\Controllers;

class LoginController extends Controller
{
    public function index()
    {
      return view('layout.login');
    }
    public function checkLogin(){
        $input = ['email'=>request('username'),'password'=>request('password'),'status'=>1];
        if(auth()->attempt($input,true)){

            return redirect()->route('dashboard')->with('message','Successfully Logged in');
        }else{
            return redirect()->route('login')->with('message','Invalid Password, Please try to login again');
        }
    }
    public function logout(){
        auth()->guard('admin')->logout();
        return redirect()->route('login')->with('message','Successfully Logged Out!!');
    }
}
