<?php

namespace App\Http\Controllers;
use App\Models\Admin;
use App\Models\HotelTable;
use App\Models\AdminFridge;
use App\Models\FridgeTemperature;

class RegistrationController extends Controller
{
    public function index()
    {
      return view('layout.register');
    }
    public function userSave()
    {
        $validator = request()->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required'
        ]);
        $input = request()->except(['_token']);
        $input['password'] =  bcrypt(request('password'));
        $input['status'] = 1;
        $newUser = Admin::create($input);
        return redirect()->route('login')->with('message','Successfully Registered');
    }
    public function dashboard()
    {
        return view('layout.dashboard');
    }
    





}
