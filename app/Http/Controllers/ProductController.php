<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\AssignedProduct;

class ProductController extends Controller
{
    public function index()
    {
        $products=Product::where('status',1)->where('created_by',auth()->user()->admin_id)->get();
        return view('products.index',compact('products'));
    }
    public function productsSave()
    {
        $validator = request()->validate([
            'product_name' => 'required',
            'product_code' => 'required',
            'product_capacity' => 'required'
        ]);
       $input = request()->except(['_token']);
       $input['created_by'] =  auth()->user()->admin_id;
       $input['inventory_id'] = "INT".sprintf("%04d",Product::max('product_id')+1);
       $newFridge = Product::create($input);
       return redirect()->route('products')->with('message','Product Added Successfully ');
    }
    public function myProducts()
    {
        $myProducts=AssignedProduct::where('status',1)->where('inventory_id',2)->get();
        return view('products.shopProducts',compact('myProducts'));
    }
}
