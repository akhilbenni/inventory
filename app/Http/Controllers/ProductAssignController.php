<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Admin;
use App\Models\AssignedProduct;
use App\Models\ShopProduct;

class ProductAssignController extends Controller
{
    public function index()
    {
        $products=Product::where('status',1)->where('product_capacity','>',0)->get();
        $admins=Admin::where('status',1)->where('role',1)->get();
        $assignedProducts=ShopProduct::where('status',1)->get();
        return view('products.assignProducts',compact('products','admins','assignedProducts'));
    }

    public function assignProductSave()
    {
        $validator = request()->validate([
            'product_id' => 'required',
            'inventory_id' => 'required',
            'qty' => 'required'
        ]);
        $input = request()->except(['_token']);
        $product=Product::where('status',1)->where('product_id',request('product_id'))->first();
        if($product){
            if($product->product_capacity >= request('qty')){
                $total_exist=ShopProduct::where('product_id',request('product_id'))->where('inventory_id',request('inventory_id'))->first();
                if($total_exist){
                    ShopProduct::where('product_id',request('product_id'))->where('inventory_id',request('inventory_id'))->update(['quantity' =>(request('qty')+$total_exist->quantity)]);
                }else{
                    $input['created_by'] =  auth()->user()->admin_id;
                    $input['quantity'] =  request('qty');
                    $input['inventory_id'] =  request('inventory_id');
                    $input['product_id'] =  request('product_id');
                    $assignedProduct = ShopProduct::create($input);
                }
                $masterProduct=Product::where('product_id',request('product_id'))->decrement('product_capacity', request('qty'));

                return redirect()->route('assign.product')->with('message','Product assigned successfully');
            }else{
                return redirect()->route('assign.product')->with('error','No Product Quantity Found');
            }
        }else{
            return redirect()->route('assign.product')->with('error','No product Found');
        }
        
        
    }
    public function purchaseProducts()
    {
        $products=ShopProduct::where('status',1)->where('quantity','>',0)->get();
        $admins=Admin::where('status',1)->where('role',1)->get();
        
        return view('products.userProducts',compact('products','admins'));
    }
    public function getShopProducts()
    {
        $products=ShopProduct::with('product')->where('status',1)->where('inventory_id',request('inventory_id'))->where('quantity','>',0)->get();
        return response()->json(['status' => 200,'data'=>$products ,'message' => "Login Success"]);
    }
    public function purchaseShopProductSave()
    {
        $validator = request()->validate([
            'product_id' => 'required',
            'from_inventory_id' => 'required',
            'to_inventory_id' => 'required',
            'qty' => 'required'
        ]);
        $shopProduct=ShopProduct::where('status',1)->where('inventory_id',request('from_inventory_id'))->where('product_id',request('product_id'))->first();
        if($shopProduct){
            if($shopProduct->quantity >= request('qty')){
                $total_exist=ShopProduct::where('product_id',request('product_id'))->where('inventory_id',request('to_inventory_id'))->first();
                if($total_exist){
                    ShopProduct::where('product_id',request('product_id'))->where('inventory_id',request('to_inventory_id'))->increment('quantity',request('qty'));
                }else{
                    $input['created_by'] =  auth()->user()->admin_id;
                    $input['quantity'] =  request('qty');
                    $input['inventory_id'] =  request('to_inventory_id');
                    $input['product_id'] =  request('product_id');
                    $assignedProduct = ShopProduct::create($input);
                }
                $masterProduct=ShopProduct::where('product_id',request('product_id'))->where('inventory_id',request('from_inventory_id'))->decrement('quantity', request('qty'));

                return redirect()->route('user.purchase.products')->with('message','Product assigned successfully');
            }else{
                return redirect()->route('user.purchase.products')->with('error','No Product Quantity Found');
            }
        }else{
            return redirect()->route('user.purchase.products')->with('error','No product Found');
        }
    }
}
